<?php

namespace Shc\Support\Pagination;

use Illuminate\Http\Client\Response;
use JsonSerializable;

abstract class ApiPaginator implements JsonSerializable
{
    abstract static function create(PromiseInterface|Response $response, int $perPage): self;
}