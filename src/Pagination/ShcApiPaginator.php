<?php

namespace Shc\Support\Pagination;

use Illuminate\Http\Client\Response;
use Illuminate\Routing\Route;
use Illuminate\Support\Arr;
use JsonSerializable;

class ShcApiPaginator extends ApiPaginator
{
    public function __construct(
        protected PromiseInterface|Response $response,
        protected int $currentPage,
        protected int $perPage,
    )
    {
    }

    public static function create(PromiseInterface|Response $response, int $perPage): self
    {
        return new self(
            $response,
            request()->query('page') ?? 1,
            $perPage
        );
    }

    public function body()
    {
        return Arr::get(json_decode($this->response->body(), true), 'data');
    }

    public function total()
    {
        return count($this->body());
    }

    public function lastPage()
    {
        return $this->total() % 10 === 0 ? (int)($this->total() / $this->perPage) : (int)($this->total() / $this->perPage) + 1;
    }

    public function links()
    {
        $links = [];

        $links[] = [
            'url' => $this->currentPage !== 1 ? sprintf('%s%spage=%s', request()->fullUrl(), request()->query() ? '&' : '?' , $this->currentPage - 1) : null,
            'label' => '&laquo;',
            'active' => false,
        ];

        for ($i = 1; $i <= $this->lastPage(); $i++) {
            $links[] = [
                'url' => sprintf('%s%spage=%s', request()->fullUrl(), request()->query() ? '&' : '?', $i),
                'label' => (string) $i,
                'active' => $this->currentPage === $i,
            ];
        }

        $links[] = [
            'url' => $this->currentPage !== $this->lastPage() ? sprintf('%s%spage=%s', request()->fullUrl(), request()->query() ? '&' : '?', $this->currentPage + 1) : null,
            'label' => '&raquo;',
            'active' => false,
        ];

        return $links;
    }

    public function data()
    {
        return array_slice(
            $this->body(),
            $this->perPage * ($this->currentPage - 1),
            $this->perPage
        );
    }

    public function jsonSerialize(): mixed
    {
        return [
            'meta' => [
                'currentPage' => $this->currentPage,
                'perPage' => $this->perPage,
                'from' => $this->perPage * ($this->currentPage - 1) + 1,
                'to' => $this->currentPage !== $this->lastPage() ? $this->perPage * ($this->currentPage + 1) : $this->total(),
                'total' => $this->total(),
                'lastPage' => $this->lastPage(),
                'links' => $this->links(),
                'currentLink' => sprintf('%s%spage=%s', request()->fullUrl(), request()->query() ? '&' : '?', $this->currentPage)
            ],
            'data' => $this->data(),
        ];
    }
}