<?php

namespace Shc\Support\middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

class AuthenticateApiCall
{
    /**
     * Handle an incoming api request and validate the api key in the header.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->header('X-API-KEY') !== Config::get('microservices.globalApiKey')) {
            abort(Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}