<?php

namespace Shc\Support;

use Illuminate\Contracts\Console\Application;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider;
use Shc\Support\middleware\AuthenticateApiCall;
use Shc\Support\Services\ShcContractService;

class ShcSupportServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        if (!$this->app->runningInConsole()) {
            return;
        }

        $this->publishes([
            __DIR__ . '/../config/microservices.php' => config_path('microservices.php')
        ]);

        $kernel = $this->app->make(Kernel::class);

        $kernel->appendMiddlewareToGroup('api', AuthenticateApiCall::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/microservices.php', 'api');

        $this->app->singleton(ShcContractService::class, function () {
            return new ShcContractService();
        });
    }
}