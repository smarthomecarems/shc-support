<?php

namespace Shc\Support\Services;

use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Shc\Support\Pagination\ApiPaginator;
use Shc\Support\Pagination\ShcApiPaginator;

abstract class ApiService
{
    /**
     * @var string $currentRequestUrl
     */
    protected string $currentRequestUrl;

    /**
     * @var PromiseInterface|Response currentResponse
     */
    protected PromiseInterface|Response $currentResponse;

    public function get(): self
    {
        $this->currentResponse = $this->requestWithApiKey()->get($this->currentRequestUrl);

        return $this;
    }

    public function post(array $data): self
    {
        $this->currentResponse = $this->requestWithApiKey()->post($this->currentRequestUrl, $data);

        return $this;
    }

    public function patch(array $data): self
    {
        $this->currentResponse = $this->requestWithApiKey()->patch($this->currentRequestUrl, $data);

        return $this;
    }

    public function delete(): self
    {
        $this->currentResponse = $this->requestWithApiKey()->delete($this->currentRequestUrl);

        return $this;
    }

    /**
     * @return PromiseInterface|Response
     */
    public function response(): PromiseInterface|Response
    {
        return $this->currentResponse;
    }

    /**
     * @param int $perPage
     * @return ApiPaginator
     */
    public function paginate(int $perPage = 10): ApiPaginator
    {
        return Config::get('microservices.paginator')::create($this->requestWithApiKey()->get($this->currentRequestUrl), $perPage);
    }

    public function body(): array
    {
        return Arr::get(json_decode($this->currentResponse->body(), true), 'data');
    }

    /**
     * @return PendingRequest
     */
    private function requestWithApiKey(): PendingRequest
    {
        return Http::withHeaders([
            'X-API-KEY' => Config::get('microservices.globalApiKey'),
        ]);
    }
}