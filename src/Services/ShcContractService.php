<?php

namespace Shc\Support\Services;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class ShcContractService extends ApiService
{
    protected string $baseUrl;

    const ILLNESS = '/api/illness';
    const INSURANCE_COMPANY = '/api/insuranceCompany';

    public function __construct()
    {
        $this->baseUrl = Config::get('microservices.contract.baseUrl');
    }


    public function insuranceCompanies(?string $search = ''): self
    {
        $this->currentRequestUrl = sprintf('%s%s?s=%s', $this->baseUrl, self::INSURANCE_COMPANY, $search ?? '');
        
        return $this;
    }

    public function insuranceCompany(string $id)
    {
        $this->currentRequestUrl = sprintf('%s%s/%s', $this->baseUrl, self::INSURANCE_COMPANY, $id);

        return $this;
    }

    public function illnesses(?string $search = ''): self
    {
        $this->currentRequestUrl = sprintf('%s%s?s=%s', $this->baseUrl, self::ILLNESS, $search ?? '');

        return $this;
    }

    public function illness(string $id): self
    {
        $this->currentRequestUrl = sprintf('%s%s/%s', $this->baseUrl, self::ILLNESS, $id);

        return $this;
    }
}