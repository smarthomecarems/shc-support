<?php

use Shc\Support\Pagination\ShcApiPaginator;

return [
    'globalApiKey' => env('GLOBAL_API_KEY'),
    'base' => [
        'baseUrl' => env('SHC_BASE_URL'),
    ],
    'contract' => [
        'baseUrl' => env('SHC_CONTRACT_URL')
    ],
    'paginator' => ShcApiPaginator::class,
];
